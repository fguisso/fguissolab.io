---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Fernando Guisso.
Developer and hacker, and when I say hacker, it's not the one who breaks into computers and steals money from others, hackers just like the guys at [MIT](https://en.wikipedia.org/wiki/Hacks_at_the_Massachusetts_Institute_of_Technology#IHTFP) who modify cars and trains. IHTFP! We sometimes break into computers, but it's all for academic purposes, and we never steal money from others.

### my history

He disassembled toys and tried to improve as a child, learned from his uncle how to make sites on VillaBol and thereafter became interested in programming. Graduated as a technician in electronics, just to learn to blink a led in Arduino, which unfortunately did not learn in the course.
In childhood, he never had patience with games, always tried to hack them or outsmart other users. My first contact with information security, even without knowing that today would work doing phishing awareness campaigns.

He lives in technology events, sometimes lecturing, sometimes organizing, sometimes helping and sometimes just attending. In addition to web programming, I enjoyed a lot of bitcoin and blockchain, both the economic part of the thing and the development part, programming.

I spent time working with technology communities and today I help with what I have learned and still learn about them.
